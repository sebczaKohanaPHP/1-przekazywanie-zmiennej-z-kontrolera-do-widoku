<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {

    
	public $template = "layout";
	public function __construct(Request $request, Response $response) {
	    parent::__construct($request, $response);
	}
	
	public function before() {
	    parent::before();
	    if($this->auto_render){
		$this->template->title = '';
		$this->template->descritpion = '';
		$this->template->content = '';
		
		$this->template->styles = array();
		$this->template->scripts = array();
	    }
	}
	
	public function after() {
	    
	    if($this->auto_render){
		$styles =array(
		    '/media/css/bootstrap.css'=>'screen',
		);
		
		$scripts = array(
		    '/media/js/bootstrap.js'
		);
		$this->template->scripts = array_merge($this->template->scripts, $scripts);
		$this->template->styles = array_merge($this->template->styles, $styles);
	    }
	    parent::after();
	}
	public function action_index()
	{
		$tekst = "helo jestem seba";
		$this->template->content = View::factory('glowna')
			->set('nazwa', $tekst);
		
	}

} 
